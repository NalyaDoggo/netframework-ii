﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace WPFQuiz2
{
    class LINQ4
    {
        //The code doesn't work very well in a class. I couldn't find a console window to add in the WPF window.
        //To use in a console window, make usingPredicate accept a List<int> and pass the list below into it.
        public static List<int> numberList = new List<int> { 55, 200, 740, 76, 230, 482, 95 };

        private static void usingPredicate()
        {
            Console.WriteLine("Expected output: ");
            foreach (int i in numberList.FindAll(greater80))
            {
                Console.WriteLine(i);
            }
        }

        private static bool greater80(int num)
        {
            if (num > 80)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
