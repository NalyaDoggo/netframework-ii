﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFQuiz2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Student> students = new List<Student>();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Reset_Button_Click(object sender, RoutedEventArgs e)
        {
            IdTextBox.Text = "";
            RegisterCheckBox.IsChecked = false;
            FirstNameTextBox.Text = "";
            LastNameTextBox.Text = "";
        }

        private void Add_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Student s = new Student(Int32.Parse(IdTextBox.Text), FirstNameTextBox.Text, LastNameTextBox.Text, (bool)RegisterCheckBox.IsChecked);
                students.Add(s);
                MessageBox.Show($"Added student! Current: {students.Count}");
                StudentDataGrid.Items.Refresh();
            }
            catch
            {
                MessageBox.Show("Incorrect input format");
            }
        }

        private void StudentDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            StudentDataGrid.ItemsSource = students;
        }
    }
}
