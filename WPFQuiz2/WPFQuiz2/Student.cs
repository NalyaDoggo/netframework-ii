﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFQuiz2
{
    class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Boolean IsRegistered { get; set; }

        public Student(int id, string firstName, string lastName, bool isRegistered)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            IsRegistered = isRegistered;
        }


    }
}
